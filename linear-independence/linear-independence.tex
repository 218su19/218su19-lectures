\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\mybmat}[1]{
  \begin{bmatrix}
    #1
  \end{bmatrix}}




\title{Linear Independence}
\subtitle{Math 218}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \tableofcontents[sections={1-2}]
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}


\section{Background}
\subsection{Linear Combinations as Matrix Multiplication}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Let $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{k}}$ be a list of vectors
    in $\mathbb{R}^n$. 
    \onslide<2->{Every linear combination
      \[
      c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_k\cdot\vv*{v}{k}=\vv{b}
      \]
    is of the form $A\vv{c}=\vv{b}$ where}%
    \onslide<3->{
    \begin{align*}
      A &= \mybmat{\vv*{v}{1} & \vv*{v}{2} & \dotsb & \vv*{v}{k}} &
      \vv{c} &= \mybmat{c_1\\ c_2\\ \vdots\\ c_k} & 
      \vv{b} &= \mybmat{b_1\\ b_2\\ \vdots\\ b_n}
    \end{align*}}%
  \onslide<4->{Note that $A$ is a $n\times k$ matrix.}
  \end{block}
\end{frame}


\begin{sagesilent}
  u1 = vector([1, 31])
  u2 = vector([0, -3])
  u3 = vector([7, -5])
  u4 = vector([-5, -3])
  v1 = matrix.column([1, 31])
  v2 = matrix.column([0, -3])
  v3 = matrix.column([7, -5])
  v4 = matrix.column([-5, -3])
  A = matrix.column([u1, u2, u3, u4])
  b = matrix.column([33,-11])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The linear combination 
    \[
    c_1\cdot\sage{v1}+c_2\cdot\sage{v2}+c_3\cdot\sage{v3}+c_4\cdot\sage{v4}=\sage{b}
    \]
    may be written as\pause
    \[
    \sage{A}\mybmat{c_1\\ c_2\\ c_3\\ c_4}=\sage{b}
    \]
  \end{example}
\end{frame}


\subsection{An Important Observation}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Let $\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$ be a list of vectors in
    $\mathbb{R}^n$. The equation
    \begin{equation}
      \label{eq:linind}
      c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_k\cdot\vv*{v}{k}=\vv{O}\tag{$\ast$}
    \end{equation}
    can always be solved by $c_1=c_2=\dotsb=c_k=0$.
    \pause This is called the \emph{trivial linear combination}.
  \end{block}

  \pause
  \begin{block}{Question}
    When is \eqref{eq:linind} solved by a \emph{nontrivial linear combination}?
  \end{block}
\end{frame}


\section{Definitions and Examples}
\subsection{Definitions}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A list of vectors $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{k}}$ in
    $\mathbb{R}^n$ is \emph{linearly independent} if the only solution to
    \[
    c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_k\cdot\vv*{v}{k}=\vv{O}
    \]
    is the trivial solution $c_1=c_2=\dotsb=c_k=0$. %
    \onslide<2->{The list $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{k}}$ is
      \emph{linearly dependent} if it is not linearly independent.}
  \end{definition}


  \begin{block}{\onslide<3->{Note}}
    \onslide<3->{The list $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{k}}$ is
      \emph{linearly dependent} if the equation
    \[
    c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_k\cdot\vv*{v}{k}=\vv{O}
    \]
    has a nontrivial solution.}
  \end{block}
\end{frame}


\subsection{Examples}

% \begin{sagesilent}
%   A = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=3)
%   v1, v2, v3, v4 = [matrix.column(col) for col in A.columns()]
%   c1, c2, c3, c4 = A.right_kernel().matrix().list()
%   z = A*A.right_kernel().matrix()
% \end{sagesilent}
\begin{sagesilent}
  A = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=3)
  v1, v2, v3, v4 = [matrix.column(col) for col in A.columns()]
  c1, c2, c3, c4 = A.right_kernel().matrix().list()
  z = A * A.right_kernel().matrix().transpose()
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    \[
    (\sage{c1})\sage{v1}
    +(\sage{c2})\sage{v2}
    +(\sage{c3})\sage{v3}
    +(\sage{c4})\sage{v4}
    =\sage{z}
    \]\pause
    Thus the list
    \[
    \Set*{\sage{v1},\sage{v2},\sage{v3},\sage{v4}}
    \]
    is linearly dependent.
  \end{example}
\end{frame}


\begin{sagesilent}
  A = matrix([(-2, 5, -6), (4, 21, -75), (-1, 0, 4), (-5, -23, 86)])
  v1, v2, v3 = [matrix.column(col) for col in A.columns()]
  z = zero_vector(A.nrows())
  M = A.augment(z, subdivide=True)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}
  
  \begin{example}
    Let 
    \begin{align*}
      \vv*{v}{1} &= \sage{v1} &
      \vv*{v}{2} &= \sage{v2} &
      \vv*{v}{3} &= \sage{v3} 
    \end{align*}
    Is the list $\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}}$ linearly independent?
  \end{example}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution}
    Suppose that
    \[
    c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+c_3\cdot\vv*{v}{3}=\vv{O}
    \]\pause
    This gives the system 
    \[
    \sage{M}\pause\rightsquigarrow\sage{M.rref()}
    \]\pause
    Thus $c_1=c_2=c_3=0$. \pause Hence $\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}}$ is
    linearly independent.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose that $\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3},\vv*{v}{4}}$ is linearly
    independent. Show that $\Set{\vv*{v}{1}-\vv*{v}{4}, \vv*{v}{2}-\vv*{v}{4},
      \vv*{v}{3}-\vv*{v}{4}}$ is linearly independent.
  \end{example}
  \pause
  \begin{block}{Solution}
    Suppose that 
    \[
    c_1\cdot(\vv*{v}{1}-\vv*{v}{4})
    +c_2\cdot(\vv*{v}{2}-\vv*{v}{4})
    +c_3\cdot(\vv*{v}{3}-\vv*{v}{4})
    =\vv{O}
    \]\pause
    Then
    \begin{equation}
      c_1\cdot\vv*{v}{1}
      +c_2\cdot\vv*{v}{2}
      +c_3\cdot\vv*{v}{3}
      +(-c_1-c_2-c_3)\cdot\vv*{v}{4}
      =\vv{O}\label{eq:mypf}\tag{$\ast$}
    \end{equation}\pause
    Since $\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3},\vv*{v}{4}}$ is linearly
    independent, each coefficient in \eqref{eq:mypf} must be zero. \pause In
    particular, $c_1=c_2=c_3=0$.
  \end{block}
\end{frame}


\section{The Linear Independence Test}
\subsection{Statement}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Suppose that $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{k}}$ is linearly
    independent. \pause This means that the equation
    \begin{equation}
      c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_k\cdot\vv*{v}{k}=\vv{O}\label{eq:tingler}
      \tag{$\ast$}
    \end{equation}
    is only solved by $c_1=c_2=\dotsb=c_k=0$. \pause But the equation in
    \eqref{eq:tingler} is given by the augmented matrix
    \[
    \left[
      \begin{array}{cccc|c}
        \vv*{v}{1} & \vv*{v}{2} & \dotsb & \vv*{v}{k} & \vv{O}
      \end{array}\right]
    \]\pause
    Thus, the equation \eqref{eq:tingler} has a unique solution if and only if
    the matrix
    \[
    A=\mybmat{\vv*{v}{1} & \vv*{v}{2} & \dotsb & \vv*{v}{k}}
    \]
    has $\textnormal{rank}=\pause k$.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Linear Independence Test]
    Let $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{k}}$ be a list of vectors
    in $\mathbb{R}^n$. Let $A$ be the matrix
    \[
    A=\mybmat{\vv*{v}{1} & \vv*{v}{2} & \dotsb & \vv*{v}{k}}
    \]
    Then $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{k}}$ is linearly
    independent if and only if $\rank(A)=\#\mycolumns(A)$.
  \end{theorem}

\end{frame}


\subsection{Example}


\begin{sagesilent}
  A = random_matrix(ZZ, 4, 3, algorithm='echelonizable', rank=2)
  v1, v2, v3 = [matrix.column(col) for col in A.columns()]  
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is the list $\Set*{\sage{v1},\sage{v2},\sage{v3}}$ linearly
    independent?
  \end{example}
  \pause
  \begin{block}{Solution}
    Note that
    \[
    \rref\sage{A}=\sage{A.rref()}
    \]\pause
    Since $\textnormal{rank}=2<\#\mycolumns$, the list is not linearly
    independent.
  \end{block}
\end{frame}


\begin{sagesilent}
  A = random_matrix(ZZ, 4, 3, algorithm='echelonizable', rank=3)
  v1, v2, v3 = [matrix.column(col) for col in A.columns()]  
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is the list $\Set*{\sage{v1},\sage{v2},\sage{v3}}$ linearly
    independent?
  \end{example}
  \pause
  \begin{block}{Solution}
    Note that
    \[
    \rref\sage{A}=\sage{A.rref()}
    \]\pause
    Since $\textnormal{rank}=3=\#\mycolumns$, the list is linearly independent.
  \end{block}
\end{frame}


\begin{sagesilent}
  A = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=3)
  v1, v2, v3, v4 = [matrix.column(col) for col in A.columns()]  
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is the list $\Set*{\sage{v1},\sage{v2},\sage{v3},\sage{v4}}$ linearly
    independent?
  \end{example}
  \pause
  \begin{block}{Solution}
    The rank of 
    \[
    A=\sage{A}
    \]
    satisfies $\rank(A)\leq \pause 3$. \pause Since
    $\rank(A)\neq\#\mycolumns=4$, the list is not linearly independent.
  \end{block}
\end{frame}


\end{document}
