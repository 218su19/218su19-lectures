\documentclass[usenames,dvipsnames]{beamer}

\usepackage{mathtools} % loads amsmath
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{blkarray, bigstrut}
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{multicol}
\usepackage{hyperref}
\hypersetup{colorlinks, linkcolor=., urlcolor=blue}
\usepackage{sagetex}
\usepackage{siunitx}
\newcommand{\mydollars}[1]{\SI[round-precision=2,round-mode=places,round-integer-to-decimal]{#1}[\$]{}}
\DeclareSIUnit{\mph}{mph}
\usepackage{xparse}
\usepackage{xfrac}
% \usepackage{showframe}          % for testing
\PassOptionsToPackage{usenames, dvipsnames, svgnames}{xcolor}
\usepackage{tikz}
\usetikzlibrary{
  , arrows
  , automata
  , calc
  , cd
  , decorations
  , decorations.pathmorphing
  , decorations.pathreplacing
  , fit
  , matrix
  , positioning
  , shapes
  , shapes.geometric
}
\usepackage{tikz-3dplot}
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\text{\ding{51}}}%
\newcommand{\xmark}{\text{\ding{55}}}%



\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\boxpivot}[1]{
  \filldraw[draw=blue, thick, fill=blue!20, fill opacity=.25] (m-#1.north east) -- (m-#1.north west) -- (m-#1.south west) -- (m-#1.south east) -- cycle;
}
\newcommand{\boxcell}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-#3.north east) -- (#1-#2-#3.north west) -- (#1-#2-#3.south west) -- (#1-#2-#3.south east) -- cycle;
}
\newcommand{\boxrow}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-1.north west) -- (#1-#2-1.south west) -- (#1-#2-#3.south east) -- (#1-#2-#3.north east) -- cycle;
}
\newcommand{\boxcol}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-1-#2.north west) -- (#1-#3-#2.south west) -- (#1-#3-#2.south east) -- (#1-1-#2.north east) -- cycle;
}



\ExplSyntaxOn
\NewDocumentCommand{\gcenter}{m}
{
  \begin{center}
    \seq_set_split:Nnn \l_tmpa_seq { \\ } { #1 }
    \seq_map_inline:Nn \l_tmpa_seq
    {
      \seq_set_split:Nnn \l_tmpb_seq { & } { ##1 }
      \seq_use:Nn \l_tmpb_seq { \hfil }
      \\
    }
  \end{center}
}
\ExplSyntaxOff

\newcommand{\mybold}[1]{{\usebeamercolor[fg]{example text}{#1}}}
\newcommand{\myotherbold}[1]{{\usebeamercolor[fg]{title}{#1}}}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%
\DeclarePairedDelimiter\pair{(}{)}%

\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\LNull}{LNull}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\codim}{codim}
\DeclareMathOperator{\proj}{proj}

\setbeamertemplate{caption}{\raggedright\insertcaption\par}


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\RR}{\mathbb{R}}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}

\theoremstyle{definition}
\newtheorem{algorithm}{Algorithm}


\title{Orthonormality and the Gram–Schmidt Algorithm}
\subtitle{Math 218}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}


\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents[sections={1-3}]
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1-3}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={4-}]
  % \end{columns}
\end{frame}

\begin{frame}

  \frametitle{\secname}
  \tableofcontents[sections={4-}]

\end{frame}


\section{Preliminaries}
\subsection{Transposition and Inversion Operators}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  At this stage, we have encountered the operations of \emph{transposition} and
  \emph{inversion}. \onslide<2->{Both operations are}
  \begin{align*}
    \onslide<3->{\textnormal{\myotherbold{order-reversing}}} && \onslide<4->{(AB)^\intercal &= B^\intercal A^\intercal} & \onslide<5->{(AB)^{-1} &= B^{-1}A^{-1}} \\
    \onslide<6->{\textnormal{\myotherbold{involutions}}}     && \onslide<7->{(A^\intercal)^\intercal &= A} & \onslide<8->{(A^{-1})^{-1} &= A}
  \end{align*}
  \onslide<9->{When $A$ has full column rank, these operations help us study the
    system $A\vv{x}=\vv{b}$.}%
  \begin{description}
  \item<10->[approximations] multiply by $A^\intercal$ to obtain
    $A^\intercal A\widehat{x}=A^\intercal\vv{b}$
  \item<11->[exact solutions] multiply by $A^{-1}$ to obtain
    $A^{-1}A\vv{x}=A^{-1}\vv{b}$
  \end{description}

\end{frame}



\begin{sagesilent}
  set_random_seed(409855830)
  A = random_matrix(ZZ, 3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Problem}
    Transposition and inversion behave similarly, but one is easier to compute
    than the other.

    \pause

    \begin{block}{Transposition is Easy}
      \[
        {\sage{A}}^\intercal=\sage{A.T}
      \]
    \end{block}

    \pause

    \begin{block}{Inversion is Hard}
      \[
        {\sage{A}}^{-1}=\sage{A.inverse()}
      \]
    \end{block}
  \end{block}

\end{frame}


\subsection{$A^\intercal A=I$ Makes Life Better}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Life Would Be Better If}
    Transposition were as useful as inversion and inversion were as easy as
    transposition.
  \end{block}

  \onslide<2->
  \begin{block}{This Happens When $A^\intercal A=I$}
    Many ``difficult'' problems become ``easy.''
    \begin{description}
    \item<3->[projection]
      $P=A(A^\intercal A)^{-1}A^\intercal=AI^{-1}A^\intercal=AA^\intercal$
    \item<4->[least squares]
      $\widehat{x}=(A^\intercal
      A)^{-1}A^\intercal\vv{b}=I^{-1}A^\intercal\vv{b}=A^\intercal\vv{b}$
    \end{description}
  \end{block}

\end{frame}


\begin{sagesilent}
  A = matrix([(4/5, 12/25, 9/25), (0, 3/5, -4/5), (3/5, -16/25, -12/25)])
  a1, a2, a3 = A.columns()
  A = matrix.column([a1, a2])
  b = vector([25, 50, -100])
  bc = matrix.column(b)
\end{sagesilent}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{This is Possible}
    Consider the matrix $A$ given by
    \begin{align*}
      A &= \sage{A} & A^\intercal A &= \sage{A.T*A} = I_{\sage{A.ncols()}}
    \end{align*}\pause
    Projection onto $\Col(A)$ is given by
    \[
      P
      = AA^\intercal
      = \sage{A*A.T}
    \]\pause
    For $\vv{b}=\sage{b}$, the least squares approximate solution to
    $A\vv{x}=\vv{b}$ is $\widehat{x} = A^\intercal\vv{b} = \sage{A.T*b}$.
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Goal}
    Work with matrices $Q$ satisfying $Q^\intercal Q=I$ rather than matrices $A$
    with $A^\intercal A\neq I$.
  \end{block}

\end{frame}




\section{Orthonormal Lists}
\subsection{Geometry of Rotations and Reflections}



\begin{sagesilent}
  e1, e2, e3 = identity_matrix(3).columns()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Recall our visual representation of $\mathbb{R}^3$.
  \begin{columns}[onlytextwidth, t]
    \column{.3\textwidth}
    \[
      \tdplotsetmaincoords{70}{110}
      \begin{tikzpicture}[tdplot_main_coords, scale=4/5]
        \draw[thick,<->] (-3,0,0) -- (3,0,0);
        \draw[thick,<->] (0,-2,0) -- (0,2,0);
        \draw[thick,<->] (0,0,-2) -- (0,0,2);

        \draw[ultra thick, ->, blue] (0, 0, 0) -- (2, 0, 0) node [above left] {$\vv*{e}{1}$};
        \draw[ultra thick, ->, red] (0, 0, 0) -- (0, 4/3, 0) node [below] {$\vv*{e}{2}$};
        \draw[ultra thick, ->, violet] (0, 0, 0) -- (0, 0, 4/3) node [right] {$\vv*{e}{3}$};
      \end{tikzpicture}
    \]
    \column{.7\textwidth}\pause
    \begin{block}{Standard Basis Vectors}
      $\vv*{e}{1}=\sage{e1}\, \vv*{e}{2}=\sage{e2}\, \vv*{e}{3}=\sage{e3}$
    \end{block}\pause
    \begin{block}{Unit Vectors}
      $\norm{\vv*{e}{1}}=1\quad \norm{\vv*{e}{2}}=1\quad \norm{\vv*{e}{3}}=1$
    \end{block}\pause
    \begin{block}{Mutually Orthogonal}
      $\vv*{e}{1}\cdot\vv*{e}{2}=0\quad \vv*{e}{1}\cdot\vv*{e}{3}=0\quad \vv*{e}{2}\cdot\vv*{e}{3}=0$
    \end{block}

  \end{columns}

\end{frame}




\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Now, suppose that we \emph{rotate} the axes.
  \newcommand{\myOld}{
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[tdplot_main_coords]
      \draw[thick,<->] (-3,0,0) -- (3,0,0);
      \draw[thick,<->] (0,-2,0) -- (0,2,0);
      \draw[thick,<->] (0,0,-2) -- (0,0,2);

      \draw[ultra thick, ->, blue] (0, 0, 0) -- (2, 0, 0) node [above left] {$\vv*{e}{1}$};
      \draw[ultra thick, ->, red] (0, 0, 0) -- (0, 4/3, 0) node [below] {$\vv*{e}{2}$};
      \draw[ultra thick, ->, violet] (0, 0, 0) -- (0, 0, 4/3) node [right] {$\vv*{e}{3}$};
    \end{tikzpicture}
  }
  \newcommand{\myNew}{
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[tdplot_main_coords, rotate=30]
      \draw[thick,<->] (-3,0,0) -- (3,0,0);
      \draw[thick,<->] (0,-2,0) -- (0,2,0);
      \draw[thick,<->] (0,0,-2) -- (0,0,2);

      \draw[ultra thick, ->, blue] (0, 0, 0) -- (2, 0, 0) node [above left] {$\vv*{q}{1}$};
      \draw[ultra thick, ->, red] (0, 0, 0) -- (0, 4/3, 0) node [below] {$\vv*{q}{2}$};
      \draw[ultra thick, ->, violet] (0, 0, 0) -- (0, 0, 4/3) node [right] {$\vv*{q}{3}$};
    \end{tikzpicture}
  }
  \[
    \begin{tikzcd}[column sep=large]
      \myOld\arrow[]{r}{\textnormal{rotate}} \pgfmatrixnextcell\myNew
    \end{tikzcd}
  \]\pause
  After rotation, $\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}$ become
  $\Set{\vv*{q}{1},\vv*{q}{2},\vv*{q}{3}}$.

\end{frame}




\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Or, suppose that we \emph{reflect} the axes.
  \newcommand{\myOld}{
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[tdplot_main_coords]
      \draw[thick,<->] (-3,0,0) -- (3,0,0);
      \draw[thick,<->] (0,-2,0) -- (0,2,0);
      \draw[thick,<->] (0,0,-2) -- (0,0,2);

      \draw[ultra thick, ->, blue] (0, 0, 0) -- (2, 0, 0) node [above left] {$\vv*{e}{1}$};
      \draw[ultra thick, ->, red] (0, 0, 0) -- (0, 4/3, 0) node [below] {$\vv*{e}{2}$};
      \draw[ultra thick, ->, violet] (0, 0, 0) -- (0, 0, 4/3) node [right] {$\vv*{e}{3}$};
    \end{tikzpicture}
  }
  \newcommand{\myNew}{
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[tdplot_main_coords]
      \draw[thick,<->] (-3,0,0) -- (3,0,0);
      \draw[thick,<->] (0,-2,0) -- (0,2,0);
      \draw[thick,<->] (0,0,-2) -- (0,0,2);

      \draw[ultra thick, ->, blue] (0, 0, 0) -- (-2, 0, 0) node [above left] {$\vv*{q}{1}$};
      \draw[ultra thick, ->, red] (0, 0, 0) -- (0, 4/3, 0) node [below] {$\vv*{q}{2}$};
      \draw[ultra thick, ->, violet] (0, 0, 0) -- (0, 0, -4/3) node [right] {$\vv*{q}{3}$};
    \end{tikzpicture}
  }
  \[
    \begin{tikzcd}[column sep=large]
      \myOld\arrow[]{r}{\textnormal{reflect}} \pgfmatrixnextcell \myNew
    \end{tikzcd}
  \]\pause
  After reflection, $\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}$ become
  $\Set{\vv*{q}{1},\vv*{q}{2},\vv*{q}{3}}$.

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Rotation and Reflection Preserve Length}
    We have not ``stretched'' the axes, so
    \begin{align*}
      \norm{\vv*{q}{1}} &= 1 & \norm{\vv*{q}{2}} &= 1 & \norm{\vv*{q}{3}} &= 1
    \end{align*}\pause
    Each of $\Set{\vv*{q}{1}, \vv*{q}{2}, \vv*{q}{3}}$ is a unit vector.
  \end{block}

  \pause
  \begin{block}{Rotation and Reflection Preserve Angles}
    We have not altered any angles, so
    \begin{align*}
      \vv*{q}{1}\cdot\vv*{q}{2} &= 0 & \vv*{q}{1}\cdot\vv*{q}{3} &= 0 & \vv*{q}{2}\cdot\vv*{q}{3} &= 0
    \end{align*}\pause
    The list $\Set{\vv*{q}{1}, \vv*{q}{2}, \vv*{q}{3}}$ is \emph{mutually
      orthogonal}.
  \end{block}

\end{frame}


\subsection{Definition of Orthonormal}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  A Mutually orthogonal list of unit vectors is called \emph{orthonormal}.

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A list of vectors $\Set{\vv*{q}{1},\vv*{q}{2},\dotsc,\vv*{q}{k}}$ is called
    \emph{orthonormal} if
    \[
      \vv*{q}{i}\cdot\vv*{q}{j}
      =
      \begin{cases}
        1 & i=j \\
        0 & i\neq j
      \end{cases}
    \]\pause
    This means that $\Set{\vv*{q}{1},\vv*{q}{2},\dotsc,\vv*{q}{k}}$ is a
    mutually orthogonal list of unit vectors.
  \end{definition}

\end{frame}




\begin{sagesilent}
  Q1 = matrix([(3/5, -4/5), (4/5, 3/5)])
  a1, a2 = map(matrix.column, Q1.columns())
  Q2 = matrix([(11/15, 2/3), (2/15, -1/3), (2/3, -2/3)])
  b1, b2 = map(matrix.column, Q2.columns())
  Q3 = matrix([(4/9, 7/9, -4/9), (1/9, 4/9, 8/9), (-8/9, 4/9, -1/9)])
  c1, c2, c3 = map(matrix.column, Q3.columns())
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Each of the lists
    \begin{align*}
      \Set*{\sage{a1}, \sage{a2}} && \Set*{\sage{b1}, \sage{b2}}
    \end{align*}
    is orthonormal.
  \end{example}

\end{frame}



\subsection{Matrices with Orthonormal Columns}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myQ}{
    \left[
      \begin{array}{ccc}
        \vv*{q}{1} & \vv*{q}{2} & \vv*{q}{3}
      \end{array}
    \right]
  }
  \newcommand{\myQT}{
    \left[
      \begin{array}{ccc}
        \vv*{q}{1} \\ \vv*{q}{2} \\ \vv*{q}{3}
      \end{array}
    \right]
  }
  \newcommand{\myQTQ}{
    \left[
      \begin{array}{ccc}
        \vv*{q}{1}\cdot\vv*{q}{1} & \vv*{q}{1}\cdot\vv*{q}{2} & \vv*{q}{1}\cdot\vv*{q}{3}  \\
        \vv*{q}{2}\cdot\vv*{q}{1} & \vv*{q}{2}\cdot\vv*{q}{2} & \vv*{q}{2}\cdot\vv*{q}{3}  \\
        \vv*{q}{3}\cdot\vv*{q}{1} & \vv*{q}{3}\cdot\vv*{q}{2} & \vv*{q}{3}\cdot\vv*{q}{3}
      \end{array}
    \right]
  }
  The letter $Q$ is typically used to denote a matrix with orthonormal
  columns. \onslide<2->{For $Q=\myQ$, the Gramian $Q^\intercal Q$ is}
  \begin{align*}
    \onslide<3->{Q^\intercal Q
    &=} \onslide<4->{\myQT\myQ \\
    &=} \onslide<5->{\myQTQ \\
    &=} \onslide<6->{\sage{identity_matrix(3)} \\
    &=} \onslide<7->{I}
  \end{align*}
  \onslide<8->{Matrices with orthonormal columns satisfy $Q^\intercal Q=I$.}


\end{frame}




\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    A matrix $Q$ has orthonormal columns if and only if $Q^\intercal Q=I$.
  \end{theorem}

  \onslide<2->
  \begin{theorem}
    Suppose that $Q$ has orthonormal columns. Then
    \begin{description}
    \item<3->[projection] $P=Q(Q^\intercal Q)^{-1}Q^\intercal=QI^{-1}Q^\intercal=QQ^\intercal$
    \item<4->[least-squares] $\widehat{x}=(Q^\intercal Q)^{-1}Q^\intercal\vv{b}=I^{-1}Q^\intercal\vv{b}=Q^\intercal\vv{b}$
    \end{description}
  \end{theorem}

  \onslide<5->
  \begin{theorem}
    Suppose that $Q$ has orthonormal columns. Then $Q$ preserves lengths.
  \end{theorem}
  \onslide<6->
  \begin{proof}
    $\norm{Q\vv{v}}^2=\onslide<7->(Q\vv{v})^\intercal(Q\vv{v})=\onslide<8->\vv{v}^\intercal Q^\intercal Q\vv{v}=\onslide<9->\vv{v}^\intercal\vv{v}=\onslide<10->\norm{\vv{v}}^2$
  \end{proof}

\end{frame}




\begin{sagesilent}
  Q_unscaled = 3*matrix([(-2/3, -1/3), (-2/3, 2/3), (-1/3, -2/3)])
  scale = 1/3
  Q = scale*Q_unscaled
  b = vector([-81, 9, 27])
  bc = matrix.column(b)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The matrix
    \[
      Q = \sage{scale}\sage{Q_unscaled}
    \]
    has orthonormal columns. \pause The projection of $\vv{b}=\sage{b}$ onto $\Col(Q)$
    is
    \[
      P\vv{b}
      = QQ^\intercal\vv{b}
      = \sage{scale**2}\sage{Q_unscaled*Q_unscaled.T}\sage{bc}
      = \sage{Q*Q.T * bc}
    \]\pause
    Note that $P\vv{b}\neq\vv{b}$, so $\vv{b}\notin\Col(Q)$.
  \end{example}

\end{frame}


\subsection{Orthogonal Matrices}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A matrix $Q$ is \emph{orthogonal} if $Q^\intercal=Q^{-1}$.
  \end{definition}

  \pause

  \begin{block}{Note}
    Orthognal matrices are the $n\times n$ matrices whose columns form an
    \emph{orthonormal basis} of $\mathbb{R}^n$.
  \end{block}

\end{frame}



\begin{sagesilent}
  var('theta')
  R = matrix([(cos(theta), sin(theta)), (-sin(theta), cos(theta))])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the matrix $R_\theta$ given by
  \[
    R_\theta=\sage{R}
  \]\pause
  This is an orthogonal matrix since
  \[
    R_\theta^\intercal R_\theta
    = \sage{R.T}\sage{R}
    = \sage{(R.T*R).simplify_trig()}
  \]\pause
  We call $R_\theta$ a \emph{rotation matrix}.
  \newcommand{\myOld}{
    \begin{tikzpicture}[line join=round, line cap=round]
      \coordinate (O) at (0, 0);

      \draw[thick,<->] (-3/2,0) -- (3/2,0);
      \draw[thick,<->] (0,-3/2) -- (0,3/2,0);

      \draw[ultra thick, ->, blue] (O) -- (1, 0) node[below] {$\vv*{e}{1}$};
      \draw[ultra thick, ->, red] (O) -- (0, 1) node[right] {$\vv*{e}{2}$};
    \end{tikzpicture}
  }
  \newcommand{\myNew}{
    \begin{tikzpicture}[line join=round, line cap=round]
      \coordinate (O) at (0, 0);

      \draw[thick,<->] (-3/2,0) -- (3/2,0);
      \draw[thick,<->] (0,-3/2) -- (0,3/2,0);

      \draw[ultra thick, ->, blue] (O) -- ($ sqrt(3)*(0, 1/2)+(1/2, 0)$) node[right] {$R_\theta\vv*{e}{1}$};
      \draw[ultra thick, ->, red] (O) -- ($ sqrt(3)*(-1/2, 0)+(0, 1/2)$) node[above] {$R_\theta\vv*{e}{2}$};
    \end{tikzpicture}
  }
  \[
    \begin{tikzcd}[column sep=large]
      \myOld\arrow[]{r}{R_\theta} \pgfmatrixnextcell \myNew
    \end{tikzcd}
  \]

\end{frame}




\begin{sagesilent}
  set_random_seed(409824)
  P1 = Permutations(3).random_element().to_matrix()
  P2 = Permutations(4).random_element().to_matrix()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Every permutation matrix is an orthogonal matrix.
    \begin{align*}
      \sage{P1} && \sage{P2}
    \end{align*}
  \end{example}

\end{frame}




\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose $\vv{u}$ is a unit vector and let
    $Q=I-2\,\vv{u}\vv{u}^\intercal$. \onslide<2->Then
    \begin{align*}
      \onslide<3->{Q^\intercal Q
      &=} \onslide<4->{(I-2\,\vv{u}\vv{u}^\intercal)^\intercal(I-2\,\vv{u}\vv{u}^\intercal) \\
      &=} \onslide<5->{(I^\intercal - 2\,(\vv{u}\vv{u}^\intercal)^\intercal)(I-2\,\vv{u}\vv{u}^\intercal) \\
      &=} \onslide<6->{(I - 2\,\vv{u}\vv{u}^\intercal)(I-2\,\vv{u}\vv{u}^\intercal) \\
      &=} \onslide<7->{I-2\,\vv{u}\vv{u}^\intercal-2\,\vv{u}\vv{u}^\intercal+4\vv{u}\vv{u}^\intercal\vv{u}\vv{u}^\intercal \\
      &=} \onslide<8->{I-4\,\vv{u}\vv{u}^\intercal+4\vv{u}\vv{u}^\intercal \\
      &=} \onslide<9->{I}
    \end{align*}
    \onslide<10->{This means that $Q$ is orthogonal. We call $Q$ a \emph{reflection matrix}.}
  \end{example}

\end{frame}


\begin{sagesilent}
  u = vector([3, 6, 2])
  u = u.normalized()
  uc = matrix.column(u)
  I = identity_matrix(u.length())
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    For $\vv{u}=\sage{u}$, we have
    \begin{align*}
      Q
      &= I-2\,\vv{u}\vv{u}^\intercal \\
      &= \sage{I}-2\sage{uc}\sage{uc.T} \\
      &= \sage{I-2*uc*uc.T}
    \end{align*}
  \end{example}

\end{frame}




\section{The Gram-Schmidt Algorithm}
\subsection{Statement}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Given a vector subspace $V\subset\mathbb{R}^n$, how do we find an
    orthonormal basis of $V$?
  \end{block}

  \pause
  \begin{block}{Answer}
    There are several algorithms that produce orthonormal bases. One such
    algorithm is the \emph{Gram-Schmidt algorithm}.
  \end{block}

  \pause
  \begin{block}{Vector Projection Formula}
    $\displaystyle\proj_{\vv{w}}(\vv{v})=\frac{\vv{w}\cdot\vv{v}}{\vv{w}\cdot\vv{w}}\vv{w}$
  \end{block}

\end{frame}




\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{algorithm}[The Gram-Schmidt Algorithm]
    Let $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{d}}$ be a basis of
    $V\subset\mathbb{R}^n$. \onslide<2->{Define new vectors}
    \newcommand{\myNormGS}[1]{\vv*{q}{####1} &= \oldfrac{\vv*{w}{####1}}{\norm{\vv*{w}{####1}}}}
    \newcommand{\myProjGS}[2]{\proj_{\vv*{w}{####1}}(\vv*{v}{####2})}
    \begin{align*}
      \onslide<3->{\vv*{w}{1} &= \vv*{v}{1}                                                          & \myNormGS{1}} \\
      \onslide<4->{\vv*{w}{2} &= \vv*{v}{2}-\myProjGS{1}{2}                                          & \myNormGS{2}} \\
      \onslide<5->{\vv*{w}{3} &= \vv*{v}{3}-\myProjGS{1}{3}-\myProjGS{2}{3}                          & \myNormGS{3}} \\
      \onslide<6->{&\vdots                                                                && \vdots}      \\
      \onslide<7->{\vv*{w}{d} &= \vv*{v}{d}-\myProjGS{1}{d}-\dotsb-\myProjGS{d-1}{d} & \myNormGS{d}}
    \end{align*}
    \onslide<8->{Then $\Set{\vv*{w}{1}, \vv*{w}{2}, \dotsc, \vv*{w}{d}}$ is a
      \emph{mutually orthogonal} basis of $V$} \onslide<9->{and
      $\Set{\vv*{q}{1}, \vv*{q}{2}, \dotsc, \vv*{q}{d}}$ is an
      \emph{orthonormal} basis of $V$.}
  \end{algorithm}

\end{frame}



\subsection{Example}


\begin{sagesilent}
  A = matrix.column([(1, 1, 0), (2, 2, 3)])
  v1, v2 = A.columns()
  Q, R = A.T.gram_schmidt()
  w1, w2 = Q.rows()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Use the Gram-Schmidt algorithm to find an orthonormal basis of $\Col(A)$ where
  \[
    A = \sage{A}
  \]
  \onslide<2->{First, we have $\vv*{w}{1}=\vv*{v}{1}=\sage{v1}$.}
  \onslide<3->{To compute $\vv*{w}{2}$, note that}
  \begin{align*}
    \onslide<4->{\vv*{w}{2}
    &=} \onslide<5->{\vv*{v}{2}-\proj_{\vv*{w}{1}}(\vv*{v}{2})
      =} \onslide<6->{\vv*{v}{2}-\frac{\vv*{w}{1}\cdot\vv*{v}{2}}{\vv*{w}{1}\cdot\vv*{w}{1}}\vv*{w}{1} \\
    &=} \onslide<7->{\sage{v2}-\oldfrac{\sage{w1}\cdot\sage{v2}}{\sage{w1}\cdot\sage{w1}}\sage{w1} \\
    &=} \onslide<8->{\sage{v2}-\oldfrac{\sage{w1*v2}}{\sage{w1*w1}}\sage{w1}
      =} \onslide<9->{\sage{w2}}
  \end{align*}
  \onslide<10->{This gives
    $\displaystyle\vv*{q}{1}=\sage{w1.norm().power(-1, hold=True)}\sage{w1}$ and
    $\vv*{q}{2}=\sage{w2.normalized()}$.}
\end{frame}



\section{$QR$-Factorizations}

\subsection{Definition}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A $QR$-factorization of an $m\times n$ matrix $A$ is an equation $A=QR$ where $Q$ has
    orthonormal columns and $R$ is upper-triangular.
  \end{definition}

\end{frame}

\begin{sagesilent}
  Q1 = matrix([(-1/9, -8/9), (-8/9, -1/9), (4/9, -4/9)])
  R1 = matrix([(9, 27), (0, -18)])
  A1 = Q1*R1
  Q2 = matrix([(-2/3, -2/3, -1/3), (-2/3, 1/3, 2/3), (1/3, -2/3, 2/3)])
  R2 = matrix([(3, -3, 18), (0, -6, 9), (0, 0, -27)])
  A2 = Q2*R2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Each of the following are $QR$-factorizations
    \begin{align*}
      \underset{A}{\sage{A1}} &= \underset{Q}{\sage{Q1}}\underset{R}{\sage{R1}} \\
      \onslide<2->{\underset{A}{\sage{A2}} &= \underset{Q}{\sage{Q2}}\underset{R}{\sage{R2}}}
    \end{align*}
  \end{example}

\end{frame}


\subsection{Properties}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    If $A=QR$, then $A$ and $R$ have the same Gramian.
  \end{theorem}

  \pause
  \begin{proof}
    $
    A^\intercal A
    = \pause (QR)^\intercal(QR)
    = \pause (R^\intercal Q^\intercal)(QR)
    = \pause R^\intercal I R
    = \pause R^\intercal R
    $
  \end{proof}

  \pause
  \begin{theorem}
    If $A=QR$ and $A$ has full column rank, then $R$ is invertible.
  \end{theorem}

\end{frame}



\section{$A=QR$ and Least Squares}
\subsection{Reducing the Least Squares Equation}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Why are $QR$-factorizations useful?
  \end{block}

  \pause
  \begin{block}{Answer 1}
    $QR$-factorizations simplify least squares problems.
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{When $A=QR$}
    The least squares equation $A^\intercal A\widehat{x}=A^\intercal\vv{b}$
    reduces to\pause
    \[
      R^\intercal R\widehat{x}= R^\intercal Q^\intercal\vv{b}
    \]
    because \pause $A$ and $R$ have the same Gramian.
  \end{block}

  \pause
  \begin{block}{When $A$ Has Full Column Rank}
    Our new least squares equation
    $R^\intercal R\widehat{x}=R^\intercal Q^\intercal\vv{b}$ further reduces to
    \pause
    \[
      R\widehat{x}=Q^\intercal\vv{b}
    \]
    because \pause $R$ is invertible.
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $A=QR$ where $A$ has full column rank. Then the least squares
    approximate solution $\widehat{x}$ to $A\vv{x}=\vv{b}$ satisfies
    $R\widehat{x}=Q^\intercal\vv{b}$.
  \end{theorem}

  \pause
  \begin{block}{Advantage}
    The equation $R\widehat{x}=Q^\intercal\vv{b}$ is quickly solved with \pause
    ``back'' substitution. \pause This is considered the most efficient way to
    solve least squares problems.
  \end{block}

\end{frame}



\subsection{Example}

\begin{sagesilent}
  Q = matrix([(-1/3, -2/3), (-2/3, -1/3), (2/3, -2/3)])
  d = lcm(map(lambda x: x.denominator(), Q.list()))
  Qd = d*Q
  A = matrix([(-1, -2), (-2, -6), (2, 8)])
  R = matrix([(3, 10), (0, -2)])
  b = vector([2, 8, -3])
  bc = matrix.column(b)
  r1, r2 = R.rows()
  r11, r12 = r1
  _, r22 = r2
  qb1, qb2 = Q.T*b
  x = A.pseudoinverse() * b
  x1, x2 = x
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the $A=QR$ factorization and the vector $\vv{b}$ given by
    \begin{align*}
      \overset{A}{\sage{A}} &= \overset{Q}{\sage{1/d}\sage{Qd}}\overset{R}{\sage{R}} & \vv{b} &= \sage{bc}
    \end{align*}
    \onslide<2->{The reduced least squares equation $R\widehat{x}=Q^\intercal\vv{b}$ is}
    \onslide<3->
    \[
      \begin{array}{rcrcrcl}
        \sage{r11}\,x_1 &+& \sage{r12}\,x_2 &=& \sage{qb1} &\onslide<5->{\to& x_1 = \oldfrac{\sage{qb1} - (\sage{r12})(\sage{x2})}{\sage{r11}} = \sage{x1}} \\
                        & & \sage{r22}\,x_2 &=& \sage{qb2} &\onslide<4->{\to& x_2 = \oldfrac{\sage{qb2}}{\sage{r22}} = \sage{x2}}
      \end{array}
    \]
    \onslide<6->This gives $\widehat{x}=\sage{x}$.
  \end{example}

\end{frame}


\section{$A=QR$ and Projections}
\subsection{Reducing the Projection Formula}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Why are $QR$-factorizations useful?
  \end{block}

  \pause
  \begin{block}{Answer 2}
    $QR$-factorizations simplify projection problems.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{When $A=QR$}
    Our projection formula becomes
    \begin{align*}
      P
      &= \onslide<2->{ A(A^\intercal A)^{-1}A^\intercal \\
      &=} \onslide<3->{ (QR)(R^\intercal R)^{-1}(QR)^\intercal \\
      &=} \onslide<4->{ QRR^{-1}(R^\intercal)^{-1}R^\intercal Q^\intercal \\
      &=} \onslide<5->{ QQ^\intercal}
    \end{align*}
    \onslide<6-> assuming that \onslide<7-> $A$ has full column rank.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $A=QR$ where $A$ has full column rank. Then $P_{\Col(A)}=QQ^\intercal$.
  \end{theorem}

  \pause
  \begin{block}{Advantage}
    It is much easier to compute $P_{\Col(A)}=QQ^\intercal$ than
    $P_{\Col(A)}=A(A^\intercal A)^{-1}A^\intercal$.
  \end{block}

\end{frame}


\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the $QR$-factorization
    \[
      \overset{A}{\sage{A}} = \overset{Q}{\sage{1/d}\sage{Qd}}\overset{R}{\sage{R}}
    \]
    \pause Projection onto $\Col(A)$ is given by
    \[
      QQ^\intercal
      = \sage{1/d**2}\sage{Qd}\sage{Qd.T}
      = \sage{1/d**2}\sage{Qd*Qd.T}
    \]
  \end{example}

\end{frame}



\section{$A=QR$ From Gram-Schmidt}
\subsection{Statement of the Algorithm}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How do we compute $A=QR$?
  \end{block}

  \pause
  \begin{block}{Answer}
    Use the Gram-Schmidt algorithm!
  \end{block}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{algorithm}[Computing $A=QR$ with Gram-Schmidt]
    Suppose that $A$ has full column rank.
    \begin{description}
    \item<2->[Step 1] Use the Gram-Schmidt algorithm to find an orthonormal basis
      $\Set{\vv*{q}{1},\dotsc,\vv*{q}{d}}$ of $\Col(A)$.
    \item<3->[Step 2] Define $Q=\begin{bmatrix}\vv*{q}{1} & \vv*{q}{2} & \dotsb & \vv*{q}{d}\end{bmatrix}$.
    \item<4->[Step 3] Define $R=Q^\intercal A$.
    \end{description}
    \onslide<5->{This gives a $QR$-factorization $A=QR$.}
  \end{algorithm}

\end{frame}



\subsection{Example}
\begin{sagesilent}
  A = matrix.column([(1, 1, 0), (2, 2, 3)])
  v1, v2 = A.columns()
  Q, R = A.T.gram_schmidt()
  w1, w2 = Q.rows()
  a = sqrt(2)
  q1 = vector([a.power(-1, hold=True), a.power(-1, hold=True), 0])
  q2 = w2.normalized()
  Q = matrix.column([q1, q2])
  R = Q.T*A
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Previously, we used the Gram-Schmidt algorithm to find an orthonormal basis
    of $\Col(A)$ where
    \[
      A = \sage{A}
      \pause\]
    This gave the basis $\Set{\vv*{q}{1}, \vv*{q}{2}}$ where
    $\vv*{q}{1}=\sage{q1}$ and $\vv*{q}{2}=\sage{q2}$. \pause Our algorithm then gives
    $A=QR$ where
    \begin{align*}
      Q &= \sage{Q} & R &= Q^\intercal A = \sage{R}
    \end{align*}
  \end{example}

\end{frame}


\end{document}
