def my_det(A):
    if A.nrows() == 1:
        return A[0, 0]
    det = 0
    cols = range(1, A.ncols())
    a1 = A.columns()[0]
    for i, a in enumerate(a1):
        if a == 0:
            continue
        rows = range(A.ncols())
        rows.remove(i)
        Ai = A.matrix_from_rows_and_columns(rows, cols)
        det += (-1) ** i * a * my_det(Ai)
    return det
