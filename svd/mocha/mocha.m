for N = 1:50

# A = input matrix, N = number of singular values to keep
function [Uc, Sigmac, Vc] = compress_matrix(A, N)
  [U, Sigma, V] = svd(A);
Uc = U(:, 1:N);
Sigmac = Sigma(1:N, 1:N);
Vc = V(:, 1:N);
end

I = imread("mocha.jpg");

[U1c, Sigma1c, V1c] = compress_matrix(I(:,:,1), N);
[U2c, Sigma2c, V2c] = compress_matrix(I(:,:,2), N);
[U3c, Sigma3c, V3c] = compress_matrix(I(:,:,3), N);

Ic = uint8( zeros(size(I,1), size(I,2), 3) );

Ic(:,:,1) = uint8(U1c * Sigma1c * V1c');
Ic(:,:,2) = uint8(U2c * Sigma2c * V2c');
Ic(:,:,3) = uint8(U3c * Sigma3c * V3c');

mocha_name = ["mocha-color-" num2str(N) ".jpg"]

imwrite(Ic, mocha_name, 'Quality', 100)

endfor
