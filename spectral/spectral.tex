\documentclass[usenames, dvipsnames]{beamer}

\usepackage{mathtools} % loads amsmath
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{blkarray, bigstrut}
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{multicol}
\usepackage{hyperref}
\hypersetup{colorlinks, linkcolor=., urlcolor=blue}
\usepackage{sagetex}
\usepackage{siunitx}
\DeclareSIUnit{\mph}{mph}
\usepackage{xparse}
\usepackage{xfrac}
% \usepackage{showframe}          % for testing
\usepackage{tikz}
\usetikzlibrary{
  , angles
  , arrows
  , automata
  , calc
  , cd
  , decorations
  , decorations.pathmorphing
  , decorations.pathreplacing
  , fit
  , matrix
  , patterns
  , positioning
  , quotes
  , shapes
  , shapes.geometric
}
\usepackage{tikz-3dplot}
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\text{\ding{51}}}%
\newcommand{\xmark}{\text{\ding{55}}}%


\ExplSyntaxOn
\NewDocumentCommand{\gcenter}{m}
{
  \begin{center}
    \seq_set_split:Nnn \l_tmpa_seq { \\ } { #1 }
    \seq_map_inline:Nn \l_tmpa_seq
    {
      \seq_set_split:Nnn \l_tmpb_seq { & } { ##1 }
      \seq_use:Nn \l_tmpb_seq { \hfil }
      \\
    }
  \end{center}
}
\ExplSyntaxOff

\newcommand{\mybold}[1]{{\usebeamercolor[fg]{example text}{#1}}}
\newcommand{\myotherbold}[1]{{\usebeamercolor[fg]{title}{#1}}}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%
\DeclarePairedDelimiter\inner{\langle}{\rangle}%

% \newcommand{\semitransp}[2][35]{\color{fg!#1}#2}

\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\LNull}{LNull}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\gm}{gm}
\DeclareMathOperator{\am}{am}
\DeclareMathOperator{\trace}{trace}
\DeclareMathOperator{\proj}{proj}


\setbeamertemplate{caption}{\raggedright\insertcaption\par}


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\RR}{\mathbb{R}}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}


\newcommand{\smallsage}[1]{{\small\sage{#1}}}
\newcommand{\footsage}[1]{{\footnotesize\sage{#1}}}
\newcommand{\scriptsage}[1]{{\scriptstyle\sage{#1}}}
\newcommand{\tinysage}[1]{{\tiny\sage{#1}}}


\theoremstyle{definition}
\newtheorem{algorithm}{Algorithm}


\title{The Spectral Theorem}
\subtitle{Math 218}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}


\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents[sections={1-3}]
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1-4}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={5-}]
  % \end{columns}
\end{frame}



\begin{frame}
  \frametitle{Overview}
  \tableofcontents[sections={4-}]
\end{frame}




\section{Complex Numbers}
\subsection{Definition}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    We write $z\in\mathbb{C}$ to indicate that $z=a+b\,i$ where
    $a, b\in\mathbb{R}$ and $i$ is the ``imaginary unit'' satisfying $i^2=-1$.
    \begin{description}[Other description]
    \item<2->[real part] of $z$ is $\Re(z)=a$
    \item<3->[imaginary part] of $z$ is $\Im(z)=b$
    \end{description}
  \end{block}

  \onslide<4->
  \begin{example}
    The complex number $z=-3+2\,i$ has real part $\Re(z)=-3$ and imaginary part
    $\Im(z)=2$.
  \end{example}

\end{frame}

\subsection{Conjugation}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    The \emph{conjugate} of $z=a+b\,i$ is $\overline{z}=a-b\,i$.
    \begin{description}[Other description]
    \item<2->[additive] $\overline{y+z}=\overline{y}+\overline{z}$
    \item<3->[multiplicative] $\overline{y\cdot z}=\overline{y}\cdot\overline{z}$
    \item<4->[involutive] $\overline{\overline{z}}=z$
    \item<5->[real-test] $\overline{z}=z\iff z\in\mathbb{R}$
    \item<6->[norm] $\norm{z}^2=z\cdot\overline{z}=a^2+b^2$
    \item<7->[inversion] $\displaystyle z^{-1}=\frac{\overline{z}}{\norm{z}^2}$
    \end{description}
  \end{block}

\end{frame}


\subsection{The Complex Plane}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Complex numbers can be ``visualized'' as vectors in the ``complex plane.''

  \[
    \begin{tikzpicture}[line join=round, line cap=round]
      \coordinate(O) at (0, 0);
      \draw[ultra thick, <->] (-3.5, 0) -- (3.5, 0) node[right] {$\Re(z)$};
      \draw[ultra thick, <->] (0, -2.5) -- (0, 2.5) node[above] {$\Im(z)$};

      \onslide<2->
      \draw[ultra thick, ->, blue] (O) -- (3, 2) node[above] {$3+2\,i$};

      \onslide<3->
      \draw[ultra thick, ->, red] (O) -- (-1, 2) node[above] {$-1+2\,i$};

      \onslide<4->
      \draw[ultra thick, ->, violet] (O) -- (-2, -1) node[below] {$-2-i$};

      \onslide<5->
      \draw[ultra thick, ->, orange] (O) -- (2, -1) node[below] {$2-i$};
    \end{tikzpicture}
  \]

\end{frame}


\section{Complex Vectors}
\subsection{Definition}

\begin{sagesilent}
  set_random_seed(714729)
  S = GaussianIntegers(names='i')
  v = random_matrix(S, 2, 1)
  w = random_matrix(S, 3, 1)
  x = random_matrix(S, 4, 1)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{complex vector} is a list of complex numbers. \pause We write
    $\vv{v}\in\mathbb{C}^n$ to indicate that $\vv{v}$ is a list of $n$ complex
    numbers.
  \end{definition}

  \pause
  \begin{example}
    The vectors
    \begin{align*}
      \vv{v} &= \sage{v} & \vv{w} &= \sage{w} & \vv{x} &= \sage{x}
    \end{align*}
    satisfy $\vv{v}\in\pause\mathbb{C}^{\sage{v.nrows()}}$, \pause
    $\vv{w}\in\pause\mathbb{C}^{\sage{w.nrows()}}$, \pause and
    $\vv{x}\in\pause\mathbb{C}^{\sage{x.nrows()}}$.
  \end{example}

\end{frame}




\begin{sagesilent}
  set_random_seed(8897)
  S = GaussianIntegers(names='i')
  v = random_matrix(S, 3, 1)
  v = v.change_ring(S.fraction_field())
  a = (v+v.C)/2
  b = (v-v.C)/(2*I)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Every $\vv{v}\in\mathbb{C}^n$ may be written as
    $\vv{v}=\vv{a}+\vv{b}\cdot i$ where $\vv{a},\vv{b}\in\mathbb{R}^n$.
    \begin{description}[Other description]
    \item<2->[real part] of $\vv{v}$ is $\Re(\vv{v})=\vv{a}$
    \item<3->[imaginary part] of $\vv{v}$ is $\Im(\vv{v})=\vv{b}$
    \end{description}
  \end{block}

  \onslide<4->
  \begin{example}
    Note that
    \[
      \overset{\vv{v}}{\sage{v}}
      = \overset{\Re(\vv{v})}{\sage{a}}+i\overset{\Im(\vv{v})}{\sage{b}}
    \]
  \end{example}

\end{frame}


\subsection{Conjugation}
\begin{sagesilent}
  set_random_seed(20979)
  S = GaussianIntegers(names='i')
  v = random_vector(S, 2)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{conjugate} of $\vv{v}=\vv{a}+\vv{b}\cdot i$ is
    $\overline{\vv{v}}=\vv{a}-\vv{b}\cdot i$.
  \end{definition}

  \pause
  \begin{example}
    The conjugate of $\vv{v}=\sage{v}$ is
    $\overline{\vv{v}}=\sage{v.conjugate()}$.
  \end{example}

\end{frame}





\subsection{The Inner Product}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{inner product} of $\vv{v}, \vv{w}\in\mathbb{C}^n$ in
    $\mathbb{C}^n$ is $\inner{\vv{v}, \vv{w}}=\overline{\vv{v}}\cdot\vv{w}$.
  \end{definition}

\end{frame}



\begin{sagesilent}
  set_random_seed(301387)
  S = GaussianIntegers(names='i')
  n = 3
  v, w = random_matrix(S, n, 2).columns()
  v1, v2, v3 = v.conjugate()
  w1, w2, w3 = w
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the vectors $\vv{v}, \vv{w}\in\mathbb{C}^{\sage{n}}$ given by
    \begin{align*}
      \vv{v} &= \sage{v} & \vv{w} &= \sage{w}
    \end{align*}
    \onslide<2->{The inner product of $\vv{v}$ and $\vv{w}$ is}
    \begin{align*}
      \onslide<3->{\inner{\vv{v}, \vv{w}}
      &=} \onslide<4->{\overline{\sage{v}}\cdot\sage{w} \\
      &=} \onslide<5->{\sage{v.conjugate()}\cdot\sage{w} \\
      &=} \onslide<6->{(\sage{v1})(\sage{w1})+(\sage{v2})(\sage{w2})+(\sage{v3})(\sage{w3}) \\
      &=} \onslide<7->{(\sage{v1*w1})+(\sage{v2*w2})+(\sage{v3*w3}) \\
      &=} \onslide<8->{\sage{v.hermitian_inner_product(w)}}
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Properties}
    The inner product on on $\mathbb{C}^n$ satisfies the following properties.
    \begin{enumerate}
    \item<2-> $\inner{\vv{v}, \vv{w}}\in\mathbb{C}$
    \item<3-> $\inner{\vv{v}, \vv{w}}=\overline{\inner{\vv{w}, \vv{v}}}$
    \item<4-> $\inner{c\cdot\vv{v}, \vv{w}}=\overline{c}\cdot\inner{\vv{v}, \vv{w}}$
    \item<5-> $\inner{\vv{v}, c\cdot\vv{w}}=c\cdot\inner{\vv{v}, \vv{w}}$
    \item<6->
      $\inner{\vv{v}+\vv{w}, \vv{x}}=\inner{\vv{v}, \vv{x}}+\inner{\vv{w},
        \vv{x}}$
    \item<7->
      $\inner{\vv{v}, \vv{w}+\vv{x}}=\inner{\vv{v}, \vv{w}}+\inner{\vv{v},
        \vv{x}}$
    \end{enumerate}
  \end{block}

\end{frame}




\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Suppose $\vv{v}\in\mathbb{C}^n$ is given by
    $\vv{v}=\inner{v_1, v_2,\dotsc, v_n}$. Then
    \begin{align*}
      \onslide<2->{\inner{\vv{v}, \vv{v}}
      &=} \onslide<3->{\overline{\vv{v}}\cdot\vv{v} \\
      &=} \onslide<4->{\overline{v_1}\cdot v_1+\overline{v_2}\cdot v_2+\dotsb+\overline{v_n}\cdot v_n \\
      &=} \onslide<5->{\norm{v_1}^2+\norm{v_2}^2+\dotsb+\norm{v_n}^2}
    \end{align*}
    \onslide<6->{This means that $\inner{\vv{v}, \vv{v}}\in\mathbb{R}$.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{norm} of $\vv{v}\in\mathbb{C}^n$ is
    $\norm{\vv{v}}=\sqrt{\inner{\vv{v}, \vv{v}}}$.
  \end{definition}

  \onslide<2->
  \begin{block}{Properties}
    For $\vv{v}, \vv{w}\in\mathbb{R}^n$, we have
    \begin{enumerate}
    \item $\Re(\inner{\vv{v}, \vv{w}})=\norm{\vv{v}}\cdot\norm{\vv{w}}\cdot\cos(\theta)$
    \item $\vv{v}\perp\vv{w}\iff\inner{\vv{v}, \vv{w}}=0$
    \end{enumerate}
  \end{block}

\end{frame}




\section{Complex Matrices}
\subsection{Definition}

\begin{sagesilent}
  set_random_seed(879)
  S = GaussianIntegers(names='i')
  A = random_matrix(S, 3, 2)
  B = random_matrix(S, 4, 3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    We write $A\in M_{m\times n}(\mathbb{C})$ or $A\in\mathbb{C}^{m\times n}$ to
    indicate that $A$ is an $m\times n$ matrix with complex entries.
  \end{definition}

  \pause
  \begin{example}
    The matrices
    \begin{align*}
      A &= \sage{A} & B &= \sage{B}
    \end{align*}
    satisfy $A\in \pause M_{\sage{A.nrows()}\times\sage{A.ncols()}}(\mathbb{C})$
    \pause and $B\in\pause\mathbb{C}^{\sage{B.nrows()}\times\sage{B.ncols()}}$.
  \end{example}

\end{frame}



\subsection{The Conjugate Transpose}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{conjugate transpose} of $A$ is $A^\ast=(\overline{A})^\intercal$.
  \end{definition}

  \pause
  \begin{block}{Idea}
    To compute $A^\ast$, we conjugate every entry in $A$ and then transpose.
  \end{block}

  \pause
  \begin{block}{Other Notation}
    Strang writes $A^H$ instead of $A^\ast$. It's also common to see $A^\dagger$
    or even $A^+$.
  \end{block}

\end{frame}


\begin{sagesilent}
  set_random_seed(4682)
  S = GaussianIntegers(names='i')
  A = random_matrix(S, 2, 3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    \begin{align*}
      A &= \sage{A} & \onslide<2->{A^\ast} &\onslide<2->{=} \onslide<3->{\sage{A.H}}
    \end{align*}
  \end{example}

\end{frame}




\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Matrices with real entries satisfy $A^\ast=A^\intercal$.
  \end{block}

  \pause
  \begin{block}{Note}
    We never just transpose complex matrices. \pause Ever.
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Properties}
    Complex conjugation satisfies the following properties.
    \begin{enumerate}
    \item<2-> $(A+B)^\ast=A^\ast+B^\ast$
    \item<3-> $(c\cdot A)^\ast=\overline{c}\cdot A^\ast$
    \item<4-> $(AB)^\ast=B^\ast A^\ast$
    \item<5-> $(A^\ast)^\ast=A$
    \item<6-> $\det(A^\ast)=\overline{\det(A)}$
    \item<7-> $\trace(A^\ast)=\overline{\trace(A)}$
    \item<8-> $(A^\ast)^{-1}=(A^{-1})^\ast$
    \item<9-> $\lambda$ e-val of $A$ $\iff$ $\overline{\lambda}$ e-val of $A^\ast$
    \item<10-> $\inner{\vv{v}, \vv{w}}=\vv{v}^\ast\vv{w}$
    \item<11->
      $\inner{A\vv{v}, \vv{w}}=(A\vv{v})^\ast\vv{w}=\vv{v}^\ast
      A^\ast\vv{w}=\inner{\vv{v}, A^\ast\vv{w}}$
    \end{enumerate}
  \end{block}

\end{frame}




% \begin{frame}

%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \begin{block}{Note}
%     Complex matrices satisfy
%     \begin{align*}
%       \Col(A)^\perp &= \Null(A^\ast) & \Null(A)^\perp &= \Col(A^\ast)
%     \end{align*}\pause
%     When $A$ is real, this is the same as before.
%   \end{block}

% \end{frame}



\subsection{Unitary Matrices}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A matrix $Q$ is \emph{unitary} if $Q^\ast=Q^{-1}$.
  \end{definition}

  \pause
  \begin{block}{Note}
    If $Q$ is a real matrix, then $Q^\ast=Q^{-1}$ is
    $Q^\intercal=Q^{-1}$. \pause Real unitary matrices are called \pause
    \emph{orthogonal}.
  \end{block}

\end{frame}




\begin{sagesilent}
  S = GaussianIntegers(names='i')
  _, i = S.gens()
  A = matrix([(-4, 3*i), (3, 4*i)])
  s = 1/5
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ given by
    \[
      A = \sage{s}\sage{A}
    \]\pause
    Note that
    \[
      A^\ast A
      = \pause\sage{s}\sage{A.H}\sage{s}\sage{A}
      = \pause\sage{s*s}\sage{A.H*A}
      = \pause I_{\sage{A.nrows()}}
    \]\pause
    This means that $A^\ast=A^{-1}$, so $A$ is unitary.
  \end{example}

\end{frame}




\section{Hermitian Matrices}
\subsection{Definition}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A matrix $A$ is \emph{Hermitian} if $A^\ast=A$.
  \end{definition}

\end{frame}





\begin{sagesilent}
  set_random_seed(476982)
  S = GaussianIntegers(names='i')
  A = random_matrix(S, 2)
  A = (A+A.H)
  B = random_matrix(S, 3)
  B = (B+B.H)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrices $A$ and $B$ given by
    \begin{align*}
      A &= \sage{A} & B &= \sage{B}
    \end{align*}\pause
    The conjugate transposes are
    \begin{align*}
      A^\ast &= \sage{A.H} & B^\ast &= \sage{B.H}
    \end{align*}\pause
    Both $A$ and $B$ are Hermitian since $A^\ast=A$ and $B^\ast=B$.
  \end{example}

\end{frame}



\subsection{Comparison with Symmetric Matrices}

\begin{sagesilent}
  set_random_seed(1479238)
  S = GaussianIntegers(names='i')
  A = random_matrix(S, 2, x=-3, y=3)
  R = A.T*A
  C = A.conjugate_transpose()*A
  A = random_matrix(ZZ, 2, x=-3, y=3)
  L = A.T*A
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Real-symmetric matrices satisfy
  $A^\ast=(\overline{A})^\intercal=A^\intercal=A$, so we have a Venn diagram.
  \[
    \begin{tikzpicture}

      \newcommand{\radius}{3}
      \newcommand{\major}{3.5cm}
      \newcommand{\minor}{3cm}

      \coordinate (O);

      \coordinate[xshift=-.6*\minor] (ceni);
      \coordinate[xshift=.6*\minor] (cenii);

      \onslide<3->{
        \filldraw[ultra thick, draw=black, red!50] (cenii) ellipse ({\major} and {\minor});
      }

      \onslide<2->{
        \filldraw[ultra thick, draw=black, blue!50] (ceni) ellipse ({\major} and {\minor});
      }

      \draw[ultra thick] (ceni) ellipse ({\major} and {\minor});
      \draw[ultra thick] (cenii) ellipse ({\major} and {\minor});

      % \draw[ultra thick] (ceni) circle (\radius and 2cm);
      % \draw[ultra thick] (cenii) circle (\radius);

      \node[xshift=-1.7cm] at (ceni) {$\overset{\textnormal{Hermitian}}{\footsage{C}}$};

      \node[xshift=1.7cm] at (cenii)%      {right};
      {$\overset{\textnormal{Non-Real Symmetric}}{\footsage{R}}$};

      \node[] at (O) {$\overset{\textnormal{Real-Symmetric}}{\footsage{L}}$};

    \end{tikzpicture}
  \]
  \onslide<4->{Non-real symmetric matrices are not very interesting.}

\end{frame}




\section{The Spectral Theorem}
\subsection{Hermitian Matrices Have Real Eigenvalues}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The eigenvalues of a Hermitian matrix $A$ are real.
  \end{theorem}
  \onslide<2->
  \begin{proof}
    Choose $\vv{v}$ so that $A\vv{v}=\lambda\cdot\vv{v}$ and
    $\norm{v}=1$. \onslide<3->{Then}
    \begin{align*}
      \onslide<3->{\lambda}
      &\onslide<3->{=} \onslide<4->{\lambda\cdot\norm{\vv{v}}^2}               & &\onslide<8->{=} \onslide<9->{\inner{A\vv{v}, \vv{v}}} \\
      &\onslide<4->{=} \onslide<5->{\lambda\cdot\inner{\vv{v}, \vv{v}}} & &\onslide<9->{=} \onslide<10->{\inner{\lambda\cdot\vv{v}, \vv{v}}} \\
      &\onslide<5->{=} \onslide<6->{\inner{\vv{v}, \lambda\cdot\vv{v}}} & &\onslide<10->{=} \onslide<11->{\overline{\lambda}\cdot\inner{\vv{v}, \vv{v}}} \\
      &\onslide<6->{=} \onslide<7->{\inner{\vv{v}, A\vv{v}}}            & &\onslide<11->{=} \onslide<12->{\overline{\lambda}\cdot\norm{\vv{v}}^2} \\
      &\onslide<7->{=} \onslide<8->{\inner{A^\ast\vv{v}, \vv{v}}}       & &\onslide<12->{=} \onslide<13->{\overline{\lambda}}
    \end{align*}
    \onslide<14->{So, $\lambda=\overline{\lambda}$ which means
      $\lambda\in\mathbb{R}$.\qedhere}
  \end{proof}

\end{frame}




\begin{sagesilent}
  set_random_seed(789)
  S = GaussianIntegers(names='i')
  _, i = S.gens()
  A = random_matrix(ZZ, 2)
  A = A.T*A
  B = random_matrix(ZZ, 3)
  B = B.T*B-B*B.T
  C = matrix([(1, 1-3*i, -i), (1+3*i, 2, 4), (i, 4, 0)])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Since real-symmetric matrices are Hermitian, this implies that the
    eigenvalues of a real-symmetric matrix are real.
  \end{block}

  \onslide<2->
  \begin{example}
    The eigenvalues of
    \begin{align*}
      \underset{\onslide<3->{\textnormal{real-symmetric}}}{\sage{A}} && \underset{\onslide<4->{\textnormal{real-symmetric}}}{\sage{B}} && \underset{\onslide<5->{\textnormal{Hermitian}}}{\sage{C}}
    \end{align*}
    are all real.
  \end{example}

\end{frame}



\subsection{Hermitian Matrices Have Orthogonal Eigenspaces}

\begingroup
\scriptsize
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The eigenspaces of a Hermitian matrix $A$ are orthogonal to one another.
  \end{theorem}
  \onslide<2->
  \begin{proof}
    Suppose that $A\vv{v}=\alpha\cdot\vv{v}$ and $A\vv{w}=\beta\cdot\vv{w}$
    where $\alpha\neq\beta$. \onslide<3->{Then}
    \begin{align*}
      \onslide<3->{\inner{\vv{v}, \vv{w}}}
      &\onslide<3->{=} \onslide<4->{\oldfrac{\alpha-\beta}{\alpha-\beta}\cdot\inner{\vv{v}, \vv{w}}}                                                   &&\onslide<9->{=}  \onslide<10->{\oldfrac{1}{\alpha-\beta}\cdot\Set{\inner{A\vv{v}, \vv{w}}-\inner{\vv{v}, \beta\cdot\vv{w}}}} \\
      &\onslide<4->{=} \onslide<5->{\oldfrac{1}{\alpha-\beta}\cdot\inner{\vv{v}, (\alpha-\beta)\cdot\vv{w}}}                                           &&\onslide<10->{=} \onslide<11->{\oldfrac{1}{\alpha-\beta}\cdot\Set{\inner{\vv{v}, A^\ast\vv{w}}-\inner{\vv{v}, \beta\cdot\vv{w}}}} \\
      &\onslide<5->{=} \onslide<6->{\oldfrac{1}{\alpha-\beta}\cdot\inner{\vv{v}, \alpha\cdot\vv{w}-\beta\cdot\vv{w}}}                                  &&\onslide<11->{=} \onslide<12->{\oldfrac{1}{\alpha-\beta}\cdot\Set{\inner{\vv{v}, A\vv{w}}-\inner{\vv{v}, \beta\cdot\vv{w}}}} \\
      &\onslide<6->{=} \onslide<7->{\oldfrac{1}{\alpha-\beta}\cdot\Set{\alpha\cdot\inner{\vv{v}, \vv{w}}-\inner{\vv{v}, \beta\cdot\vv{w}}}}            &&\onslide<12->{=} \onslide<13->{\oldfrac{1}{\alpha-\beta}\cdot\Set{\inner{\vv{v}, \beta\cdot\vv{w}}-\inner{\vv{v}, \beta\cdot\vv{w}}}} \\
      &\onslide<7->{=} \onslide<8->{\oldfrac{1}{\alpha-\beta}\cdot\Set{\inner{\overline{\alpha}\cdot\vv{v}, \vv{w}}-\inner{\vv{v}, \beta\cdot\vv{w}}}} &&\onslide<13->{=} \onslide<14->{0} \\
      &\onslide<8->{=} \onslide<9->{\oldfrac{1}{\alpha-\beta}\cdot\Set{\inner{\alpha\cdot\vv{v}, \vv{w}}-\inner{\vv{v}, \beta\cdot\vv{w}}}}
    \end{align*}
    \onslide<15->{This shows that $\vv{v}\perp\vv{w}$. } \onslide<16->{Hence
      $E_\alpha\perp E_\beta$. \qedhere}
  \end{proof}

\end{frame}
\endgroup




\begin{sagesilent}
  A = matrix([(4, -1, -1), (-1, 4, 1), (-1, 1, 4)])
  l1, l2, l2 = A.eigenvalues()
  n = A.ncols()
  I = identity_matrix(n)
  A1 = matrix.column(A-l1*I).right_kernel().basis_matrix().T
  A2 = matrix.column(A-l2*I).right_kernel().basis_matrix().T
  v, = A1.columns()
  w, x = A2.columns()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the real-symmetric matrix given by
  \begin{align*}
    A &= \sage{A} & \operatorname{E-Vals}(A) &= \Set{\sage{l1}, \sage{l2}}
  \end{align*}\pause
  The eigenspaces are given by
  \begin{align*}
    E_{\sage{l1}} &= \Col\sage{A1} & E_{\sage{l2}} &= \Col\sage{A2}
  \end{align*}\pause
  Note that $E_{\sage{l1}}\perp E_{\sage{l2}}$ since
  \begin{align*}
    \sage{v}\cdot\sage{w} &= \sage{v*w} & \sage{v}\cdot\sage{x} &= \sage{v*x}
  \end{align*}

\end{frame}




\subsection{Hermitian Matrices Are Diagonalizable}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Every Hermitian matrix is diagonalizable.
  \end{theorem}

  \pause
  \begin{proof}
    Hard.
  \end{proof}

\end{frame}





\begin{sagesilent}
  set_random_seed(789)
  S = GaussianIntegers(names='i')
  _, i = S.gens()
  A = random_matrix(ZZ, 2)
  A = A.T*A
  B = random_matrix(ZZ, 3)
  B = B.T*B-B*B.T
  C = matrix([(1, 1-3*i, -i), (1+3*i, 2, 4), (i, 4, 0)])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Since real-symmetric matrices are Hermitian, this implies that every
    real-symmetric matrix is diagonalizable.
  \end{block}

  \onslide<2->
  \begin{example}
    Each of the matrices
    \begin{align*}
      \underset{\onslide<3->{\textnormal{real-symmetric}}}{\sage{A}} && \underset{\onslide<4->{\textnormal{real-symmetric}}}{\sage{B}} && \underset{\onslide<5->{\textnormal{Hermitian}}}{\sage{C}}
    \end{align*}
    is diagonalizable.
  \end{example}

\end{frame}



\subsection{Statement of the Theorem}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Spectral Theorem]
    Every Hermitian matrix $H$ has a factorization $H=QDQ^\ast$ where $D$ is
    real-diagonal and $Q$ is unitary.
  \end{theorem}

  \pause
  \begin{block}{Note}
    The factorization $H=QDQ^\ast$ is called a \emph{spectral decomposition}.
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myA}{\scriptsize
    \left[
      \begin{array}{ccc}
        3 & 2-i & -3\,i \\
        2+i & 0 & 1-i \\
        3\,i & 1+i & 0
      \end{array}
    \right]
  }
  \newcommand{\myQ}{\scriptsize
    \left[
      \begin{array}{ccc}
        -\oldfrac{1}{\sqrt{7}} & \oldfrac{1-21\,i}{\sqrt{728}} & \oldfrac{1+3\,i}{\sqrt{40}} \\ \\
        \oldfrac{1+2\,i}{\sqrt{7}} & \oldfrac{1-9\,i}{\sqrt{728}} & \oldfrac{-2-i}{\sqrt{40}} \\ \\
        \oldfrac{1}{\sqrt{7}} & \oldfrac{13}{\sqrt{728}} & \oldfrac{5}{\sqrt{40}}
      \end{array}
    \right]
  }
  \newcommand{\myQH}{\scriptsize
    \left[
      \begin{array}{ccc}
        -\oldfrac{1}{\sqrt{7}} & \oldfrac{1-2\,i}{\sqrt{7}} & \oldfrac{1}{\sqrt{7}} \\ \\
        \oldfrac{1+21\,i}{\sqrt{728}} & \oldfrac{1+9\,i}{\sqrt{728}} & \oldfrac{13}{\sqrt{728}} \\ \\
        \oldfrac{1-3\,i}{\sqrt{40}} & \oldfrac{-2+i}{\sqrt{40}} & \oldfrac{5}{\sqrt{40}}
      \end{array}
    \right]
  }
  \newcommand{\myD}{\scriptsize
    \left[
      \begin{array}{rrr}
        -1 & 0 & 0 \\
        0 & 6 & 0 \\
        0 & 0 & -2
      \end{array}
    \right]
  }
  \begin{example}
    Note that
    \begin{align*}
      &\overset{H}{\myA} \\
      &\qquad= \overset{Q}{\myQ}\overset{D}{\myD}\overset{Q^\ast=Q^{-1}}{\myQH}
    \end{align*}\pause
    Here, $H$ is \pause Hermitian, \pause $D$ is \pause real-diagonal, \pause
    and $Q$ is \pause unitary. \pause This is a spectral decomposition of $H$.
  \end{example}

\end{frame}



\subsection{Spectral Decomposition Algorithm}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{algorithm}[Spectral Decomposition Algorithm]
    The following steps produce a spectral decomposition $H=QDQ^\ast$ of a
    Hermitian matrix $H$.
    \begin{description}
    \item<2->[Step 1] Find bases of each eigenspace.
    \item<3->[Step 2] Use Gram-Schmidt to find \emph{orthonormal bases} of each
      eigenspace.
    \item<4->[Step 3] These new bases give the columns of $Q$.
    \end{description}
  \end{algorithm}

\end{frame}


\begin{sagesilent}
  A = matrix([(-2, 1, -1), (1, -2, -1), (-1, -1, -2)])
  l1, l2 = 0, -3
  n = A.ncols()
  I = identity_matrix(n)
  A1 = (A-l1*I).right_kernel().basis_matrix().T
  A2 = (A-l2*I).right_kernel().basis_matrix().T
  v1, v2 = A2.columns()
  w1, w2 = A2.T.gram_schmidt()[0].rows()
\end{sagesilent}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The eigenspaces of $A=\tinysage{A}$ are
  \begin{align*}
    E_{\sage{l1}} &= \Col\tinysage{A1} & E_{\sage{l2}} &= \Col\tinysage{A2}
  \end{align*}
  \onslide<2->{We can find an orthonormal basis of $E_{\sage{l2}}$ by using
    Gram-Schmidt.}
  \begin{align*}
    \onslide<3->{\vv*{w}{2}
    &=} \onslide<4->{\vv*{v}{2}-\proj_{\vv*{w}{1}}(\vv*{v}{2}) \\
    &=} \onslide<5->{\sage{v2}-\oldfrac{\sage{v1}\cdot\sage{v2}}{\sage{v1}\cdot\sage{v1}}\sage{v1} \\
    &=} \onslide<6->{\sage{1/2}\sage{2*w2}}
  \end{align*}
  \onslide<7->{This gives the spectral decomposition $A=QDQ^\intercal$ where}
  \newcommand{\myQ}{
    \left[
      \begin{array}{rcr}
        \frac{1}{\sqrt{3}} & \frac{1}{\sqrt{2}} & -\frac{1}{\sqrt{6}} \\
        \frac{1}{\sqrt{3}} & 0 & \frac{2}{\sqrt{6}} \\
        -\frac{1}{\sqrt{3}} & \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{6}}
      \end{array}
    \right]
  }
  \newcommand{\myD}{
    \left[
      \begin{array}{rrr}
        0 & 0 & 0 \\
        0 & -3 & 0 \\
        0 & 0 & -3
      \end{array}
    \right]
  }
  \begin{align*}
    \onslide<8->{Q &=} \onslide<9->{\myQ} & \onslide<10->{D &=} \onslide<11->{\myD}
  \end{align*}

\end{frame}



\begin{sagesilent}
  S = GaussianIntegers(names='i')
  _, i = S.gens()
  A = matrix([(3, 1-i), (1+i, 2)])
  l1, l2 = A.eigenvalues()
  n = A.nrows()
  I = identity_matrix(n)
  A1 = (A-l1*I).right_kernel().basis_matrix().T
  A1 = 2*A1
  A2 = (A-l2*I).right_kernel().basis_matrix().T
  v, = A1.columns()
  v1, v2 = v.conjugate()
  w, = A2.columns()
  w1, w2 = w
  D = diagonal_matrix([l1, l2])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The eigenspaces of $A=\sage{A}$ are
  \begin{align*}
    E_{\sage{l1}} &= \Col\sage{A1} & E_{\sage{l2}} &= \Col\sage{A2}
  \end{align*}
  \onslide<2-> Note that $E_{\sage{l1}}\perp E_{\sage{l2}}$ since
  \[
    \overline{\sage{v}}\cdot\sage{w}
    = (\sage{v1})\cdot(\sage{w1})+(\sage{v2})\cdot(\sage{w2})
    = \sage{v.hermitian_inner_product(w)}
  \]\onslide<3->
  Our spectral decomposition is $A=QDQ^\ast$ where
  \newcommand{\myQ}{
    \left[
      \begin{array}{cc}
        \frac{2}{\sqrt{6}} & \frac{1}{\sqrt{3}} \\
        \frac{(i+1)}{\sqrt{6}} & -\frac{(i+1)}{\sqrt{3}}
      \end{array}
    \right]
  }
  \begin{align*}
    \onslide<4->{Q &=} \onslide<5->{\myQ} & \onslide<6->{D &=} \onslide<7->{\sage{D}}
  \end{align*}

\end{frame}


\end{document}
